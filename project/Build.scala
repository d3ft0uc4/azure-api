import sbt.Keys._
import sbt._


object Resolvers {
  val typeSafeRepo = Classpaths.typesafeReleases
  val sprayRepo = "spray repo" at "http://repo.spray.io"

  def commonResolvers = Seq(typeSafeRepo, sprayRepo)
}

object Dependencies {
  private val sprayVersion = "1.3.2"
  private val akkaVersion = "2.3.9"

  val json4sJackson = "org.json4s" %% "json4s-jackson" % "3.3.0"
  val json4sExt = "org.json4s" %% "json4s-ext" % "3.3.0"

  val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaRemote = "com.typesafe.akka" %% "akka-remote" % akkaVersion
  val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % akkaVersion

  val sprayCan = "io.spray" %% "spray-can" % sprayVersion
  val sprayRouting = "io.spray" %% "spray-routing" % sprayVersion

  val azure = "com.microsoft.azure" % "azure-storage" % "4.0.0"
}

object BuildSettings {
  val Organization = "test"
  val Name = "azure-api"
  val Version = "1.0.0"
  val ScalaVersion = "2.11.8"

  val buildSettings = Seq(
    organization := Organization,
    name := Name,
    version := Version,
    scalaVersion := ScalaVersion,
    fork in Test := true,
    parallelExecution in Test := false,
    javaOptions in Test := Seq("-DSCALA_ENV=test")
  )
}

object MainBuild extends Build {

  import BuildSettings._
  import Dependencies._
  import Resolvers._

  val deps = Seq(json4sExt, json4sJackson, sprayCan, sprayRouting, akkaActor, akkaRemote, akkaSlf4j, azure)

  javacOptions ++= Seq("-encoding", "UTF-8")

  lazy val main = Project(
    Name,
    file("."),
    settings = buildSettings ++ Defaults.coreDefaultSettings ++
      Seq(
        libraryDependencies ++= deps,
        resolvers := commonResolvers
      ))
}
