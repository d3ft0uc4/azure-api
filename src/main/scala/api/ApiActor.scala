package api

import akka.actor.{ActorRefFactory, ActorLogging, Actor}
import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.Serialization.write
import spray.routing.{ValidationRejection, HttpServiceActor, ExceptionHandler, HttpService}
import spray.httpx.Json4sJacksonSupport

/**
 * Created by d3ft0uch.
 */
class ApiActor extends HttpServiceActor with ActorLogging with Json4sJacksonSupport {

  import api.DataTypes._

  override implicit def json4sJacksonFormats: Formats = DefaultFormats

  implicit val eh = exceptionHandler

  implicit val rs = spray.routing.RoutingSettings.default


  override def receive: Receive = runRoute {
    pathEndOrSingleSlash {
      getFromResource("index.html")
    } ~
      path("label") {
        respondWithMediaType(spray.http.MediaTypes.`application/json`) {
          post {
            entity(as[LabelSet]) {
              req =>
                log.debug(s"Incoming request $req")
                RequestHandler.handleUpdate(req) match {
                  case Right(_) => complete(200, write(Map("success" -> true)))
                  case Left(msg) => complete(400, write(Map("success" -> false, "msg" -> msg)))
                }
            }
          } ~ delete {
            entity(as[LabelRemove]) {
              req =>
                log.debug(s"Incoming request $req")
                RequestHandler.handleDelete(req) match {
                  case Right(_) => complete(200, write(Map("success" -> true)))
                  case Left(msg) => complete(400, write(Map("success" -> false, "msg" -> msg)))
                }
            }
          }
        }
      }
  }


  def exceptionHandler: ExceptionHandler = {
    ExceptionHandler.apply {
      case e: Exception =>
        requestUri { uri =>
          entity(as[String]) { strReq =>
            log.error(e, s"Request to $uri with body $strReq could not be handled normally case of exception")
            complete(500, write(Map("success" -> false, "msg" -> "Internal server error")))
          }
        }
    }
  }
}


