package api

import akka.actor.{Props, ActorSystem}
import akka.io.IO
import akka.routing.RoundRobinPool
import akka.util.Timeout
import spray.can.Http
import akka.pattern.ask
import scala.concurrent.duration._


object Boot {

  def main(args: Array[String]): Unit = {
    System.setProperty("config.file", "application.conf")
    startSpray
  }

  def startSpray = {
    val as = ActorSystem()
    implicit val timeout = new Timeout(20 seconds)


    implicit val system = as

    val apiActorPool = system.actorOf(RoundRobinPool(10).props(Props[ApiActor]), "service-api")
    IO(Http) ? Http.Bind(apiActorPool, interface = "localhost", port = 8080)
  }

}
