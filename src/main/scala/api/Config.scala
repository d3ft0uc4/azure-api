package api

import com.typesafe.config.ConfigFactory

/**
 * Created by d3ft0uch.
 */
object Config {
  private[this] val cfg = ConfigFactory.load()

  val connectionString =
    s"DefaultEndpointsProtocol=${cfg.getString("azure.protocol")};" +
      s"AccountName=${cfg.getString("azure.account.name")};" +
      s"AccountKey=${cfg.getString("azure.account.key")}"
}
