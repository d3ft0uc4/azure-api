package api

import com.microsoft.azure.storage.table.{TableEntity, TableOperation}
import com.microsoft.azure.storage.CloudStorageAccount

/**
 * Created by d3ft0uch.
 */

object DAO {
  private[this] val account = CloudStorageAccount.parse(Config.connectionString)
  private[this] val client = account.createCloudTableClient()

  def update[T <: TableEntity](tableName: String, entity: T) = {
    val tbl = client.getTableReference(tableName)
    val op = TableOperation.replace(entity)
    tbl.execute(op)
  }

  def delete[T <: TableEntity](tableName: String, entity: T) = {
    val tbl = client.getTableReference(tableName)
    val op = TableOperation.delete(entity)
    tbl.execute(op)
  }

}
