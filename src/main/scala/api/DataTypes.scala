package api

/**
 * Created by d3ft0uch.
 */
object DataTypes {

  case class LabelSet(account: String, label: String, value: Boolean = true, overwrite: Boolean = true, binary: Boolean = true)

  case class LabelRemove(account: String, label: String)

}
