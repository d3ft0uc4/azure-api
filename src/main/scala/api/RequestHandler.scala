package api

import scala.util.control.NonFatal

/**
 * Created by d3ft0uch.
 */
object RequestHandler {
  def handleUpdate(r: DataTypes.LabelSet): Either[String, Unit] = try {
    ???
  }
  catch {
    case NonFatal(ex) => Left(ex.getMessage)
  }

  def handleDelete(r: DataTypes.LabelRemove): Either[String, Unit] = try {
    ???
  }
  catch {
    case NonFatal(ex) => Left(ex.getMessage)
  }
}
